# -*- coding: utf-8 -*-

{
    'name': 'SMS Xác nhận',
    'summary': 'BMS SMS MySMSBrandname nhắn khi xác nhận đơn hàng, hóa đơn, thanh toán',
    'version': '1.0.1.0.0',
    'author': 'BMS TECH',
    'category': 'BMS APPS',
    'license': 'AGPL-3',
    'website': 'http://www.bmstech.io',
    'sequence': 2,
    'depends': [
        'base',
        'client_sms_brand_name'],
    'data': [
        "data/bms_sms_config_data.xml",
        "views/bms_sms_config_view.xml",
        "views/menu_view.xml"
    ],
    'demo': [
    ],
    'css': [],
    'images': [
        'static/description/icon.png',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'description': """
		BMS SMS MySMSBrandname nhắn khi xác nhận đơn hàng, hóa đơn, thanh toán
	""",
}
