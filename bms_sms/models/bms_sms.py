# -*- coding: utf-8 -*-

from odoo import fields, models, api
from jinja2 import Environment


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    # TODO onchange sol, clean delivery price

    @api.multi
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        #we will check option here
        #if oftion is enable send

        #noi dung sms sender
        scs_object = self.env['bms.sms.config'].search([("apply_to", "=", "sale_confirm")], limit=1)
        scs_content = scs_object[0].sms_content

        if scs_object.co_hieu_luc:
            template = Environment().from_string(scs_content)
            rendered_content = template.render(object=self)
            default_brandname = self.env['at.brand_name'].search([('default_brandname', '=', True)], limit=1)
            if (len(default_brandname) > 0):
                for brandname in default_brandname:

                    #Gửi tin nhắn
                    new_sms_send_id = self.env['at.send_sms'].create({"name":"Gửi tin nhắn xác nhận bán hàng",
                        "brand_name": default_brandname.id,
                        "content": rendered_content,
                        "accented": scs_object[0].accent,
                        "is_send_specific_customers": True,
                        "state": "draft"})
                    new_sms_send_id.specific_customers_list = self.partner_id
                    new_sms_send_id.action_send_sms()
            else:
                sms_brandname = self.env['at.brand_name'].search([], limit=1)
                if (len(sms_brandname) > 0):
                    for brandname in sms_brandname:

                        #Gửi tin nhắn
                        new_sms_send_id = self.env['at.send_sms'].create({"name":"Gửi tin nhắn xác nhận bán hàng",
                            "brand_name": sms_brandname.id,
                            "content": rendered_content,
                            "accented": scs_object[0].accent,
                            "is_send_specific_customers": True,
                            "state": "draft"})
                        new_sms_send_id.specific_customers_list = self.partner_id
                        new_sms_send_id.action_send_sms()
        return res



class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def action_invoice_open(self):
        res = super(AccountInvoice, self).action_invoice_open()
        scs_object = self.env['bms.sms.config'].search([("apply_to", "=", "invoice_validate")], limit=1)
        scs_content = scs_object.sms_content
        if scs_object.co_hieu_luc:
            template = Environment().from_string(scs_content)
            rendered_content = template.render(object=self)
            default_brandname = self.env['at.brand_name'].search([('default_brandname', '=', True)], limit=1)
            if (len(default_brandname) > 0):
                for brandname in default_brandname:

                    #Gửi tin nhắn
                    new_sms_send_id = self.env['at.send_sms'].create({"name":"Gửi tin nhắn xác nhận hóa đơn",
                        "brand_name": default_brandname.id,
                        "content": rendered_content,
                        "accented": scs_object[0].accent,
                        "is_send_specific_customers": True,
                        "state": "draft"})
                    new_sms_send_id.specific_customers_list = self.partner_id
                    new_sms_send_id.action_send_sms()
            else:
                sms_brandname = self.env['at.brand_name'].search([], limit=1)
                if (len(sms_brandname) > 0):
                    for brandname in sms_brandname:

                        #Gửi tin nhắn
                        new_sms_send_id = self.env['at.send_sms'].create({"name":"Gửi tin nhắn xác nhận hóa đơn",
                            "brand_name": sms_brandname.id,
                            "content": rendered_content,
                            "accented": scs_object[0].accent,
                            "is_send_specific_customers": True,
                            "state": "draft"})
                        new_sms_send_id.specific_customers_list = self.partner_id
                        new_sms_send_id.action_send_sms()
        return res

class account_payment(models.Model):
    _inherit = "account.payment"

    def action_validate_invoice_payment(self):
        res = super(account_payment, self).action_validate_invoice_payment()
        scs_object = self.env['bms.sms.config'].search([("apply_to", "=", "payment_confirm")], limit=1)
        scs_content = scs_object.sms_content
        # print("========================")
        if scs_object.co_hieu_luc:
            template = Environment().from_string(scs_content)
            rendered_content = template.render(object=self)
            # print(rendered_content)
            default_brandname = self.env['at.brand_name'].search([('default_brandname', '=', True)], limit=1)
            if (len(default_brandname) > 0):
                for brandname in default_brandname:

                    #Gửi tin nhắn
                    new_sms_send_id = self.env['at.send_sms'].create({"name":"Gửi tin nhắn xác nhận thanh toán",
                        "brand_name": default_brandname.id,
                        "content": rendered_content,
                        "is_send_specific_customers": True,
                        "accented": scs_object[0].accent,
                        "state": "draft"})
                    new_sms_send_id.specific_customers_list = self.partner_id
                    new_sms_send_id.action_send_sms()
            else:
                sms_brandname = self.env['at.brand_name'].search([], limit=1)
                if (len(sms_brandname) > 0):
                    for brandname in sms_brandname:

                        #Gửi tin nhắn
                        new_sms_send_id = self.env['at.send_sms'].create({"name":"Gửi tin nhắn xác nhận thanh toán",
                            "brand_name": sms_brandname.id,
                            "content": rendered_content,
                            "is_send_specific_customers": True,
                            "accented": scs_object[0].accent,
                            "state": "draft"})
                        new_sms_send_id.specific_customers_list = self.partner_id
                        new_sms_send_id.action_send_sms()
        # print("========================")
        return res
