# -*- coding: utf-8 -*-

from odoo import fields, models, api

class BmsSmsConfig(models.Model):
    _name = "bms.sms.config"
    _description = "BMS SMS Template"

    name = fields.Char("Tên")
    apply_to = fields.Selection([
        ('sale_confirm', 'Xác nhận bán hàng'),
        ('invoice_validate', 'Xác nhận hóa đơn'),
        ('payment_confirm', 'Xác nhận thanh toán'),
        ], string='Áp dụng cho')
    accent = fields.Boolean("Gửi có dấu")
    sms_content = fields.Text("Nội dung gửi SMS")
    co_hieu_luc = fields.Boolean("Có hiệu lực", help="Không click thì template không có hiệu lực")
