# -*- coding: utf-8 -*-

from odoo import fields, models

# khách hàng
class ResPartner(models.Model):
    _inherit = "res.partner"

    birthday = fields.Date(string='Ngày sinh')
