# -*- coding: utf-8 -*-

from odoo import fields, models, api
from unidecode import unidecode
from odoo.exceptions import UserError
from datetime import datetime, timedelta
import math
from jinja2 import Environment

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    auto_birthday_send_time = fields.Float(string = 'Thời gian gửi tin nhắn')
    is_auto_birthday = fields.Boolean(string = 'Tự động gửi tin nhắn cho khách hàng')
    auto_birthday_content = fields.Text(string = 'Nội dung tin nhắn')
    auto_birthday_accent = fields.Boolean(string = 'Có dấu', default = True)

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        params = self.env['ir.config_parameter'].sudo()
        res.update(
            auto_birthday_send_time = params.get_param('at_auto_sms.auto_birthday_send_time'),
            is_auto_birthday = params.get_param('at_auto_sms.is_auto_birthday'),
            auto_birthday_content = params.get_param('at_auto_sms.auto_birthday_content'),
            auto_birthday_accent = params.get_param('at_auto_sms.auto_birthday_accent'),
        )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        param = self.env['ir.config_parameter'].sudo()
        param.set_param('at_auto_sms.auto_birthday_send_time', self.auto_birthday_send_time)
        param.set_param('at_auto_sms.is_auto_birthday', self.is_auto_birthday)
        param.set_param('at_auto_sms.auto_birthday_content', self.auto_birthday_content)
        param.set_param('at_auto_sms.auto_birthday_accent', self.auto_birthday_accent)

        self._compute_auto_birthday_send_time()

    @api.depends('auto_birthday_send_time')
    def _compute_auto_birthday_send_time(self):
        for record in self:
            time = record.auto_birthday_send_time
            string = str(math.floor(time)) + ':' + str(round((time - math.floor(time)) * 60)) + ':00'
            send_time = (datetime.utcnow() + timedelta(days = 0)).strftime('%Y-%m-%d ' + string)
            cron_jobs = self.env['ir.cron'].sudo().search([('name', '=', 'Auto send SMS')])
            for cron in cron_jobs:
                cron.write({'nextcall': send_time})

    @api.multi
    def auto_send_sms(self):
        config = self.env['res.config.settings'].search([])
        if config:
            is_auto_birthday = config[-1]['is_auto_birthday']
            if is_auto_birthday:
                content = config[-1]['auto_birthday_content']
                accented = config[-1]['auto_birthday_accent']

                default_brandname = self.env['at.brand_name'].search([('default_brandname', '=', True)], limit=1)
                current_date = str(datetime.now().month) + "-" + str(datetime.now().day)
                customer_ids = self.env['res.partner'].sudo().search([('birthday', 'ilike', current_date)])

                template = Environment().from_string(content)

                if (len(default_brandname) > 0):

                    #Gửi tin nhắn
                    for customer_id in customer_ids:
                        if customer_id.mobile:
                            rendered_content = template.render(object=customer_id)
                            new_sms_send_id = self.env['at.send_sms'].create({
                                "name":"Chúc mừng sinh nhật khách hàng",
                                "brand_name": default_brandname.id,
                                "content": rendered_content,
                                "accented": accented,
                                "is_send_specific_customers": True,
                                "state": "draft"})
                            new_sms_send_id.specific_customers_list = customer_id
                            new_sms_send_id.action_send_sms()
                else:
                    default_brandname = self.env['at.brand_name'].search([], limit=1)

                        #Gửi tin nhắn
                    for customer_id in customer_ids:
                        if customer_id.mobile:
                            rendered_content = template.render(object=customer_id)
                            new_sms_send_id = self.env['at.send_sms'].create({
                                "name":"Chúc mừng sinh nhật"+ str(datetime.today().date()),
                                "brand_name": default_brandname.id,
                                "content": rendered_content,
                                "accented": accented,
                                "is_send_specific_customers": True,
                                "state": "draft"})
                            new_sms_send_id.specific_customers_list = customer_id
                            new_sms_send_id.action_send_sms()
