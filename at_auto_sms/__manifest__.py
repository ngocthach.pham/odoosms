# -*- coding: utf-8 -*-
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).
{
    "name": 'SMS Tự động nhắn tin',
    "summary": 'Tư động Chúc mừng sinh nhật khách hàng',
    'version': '1.0.1.0.0',
    'author': 'BMS TECH',
    'category': 'BMS APPS',
    'license': 'AGPL-3',
    'website': 'http://www.bmstech.io',
    'sequence': 3,
    "application": True,
    "installable": True,
    "depends": ['base',
                'client_sms_brand_name',
                ],
    "data": [
        'views/sms_setting_view.xml',
        'views/auto_birthday.xml',
        'views/bms_res_partner.xml',
    ],
}
