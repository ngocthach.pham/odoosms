# -*- coding: utf-8 -*-
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).
{
    "name": 'ODOO SMS Brandname',
    "summary": 'Gửi tin nhắn thương hiệu với ODOO',
    "version": "1.0.0.1",
    "category": "BMS APPS",
    "website": "http://www.bmstech.io",
    "author": "BMS TECH",
    "license": "LGPL-3",
    "application": True,
    "installable": True,
    'sequence': 1,
    "depends": ['base',
                'sale',
                'hr'
                ],
    "data": [
        'security/at_sms_brandname_client_group.xml',
        'security/at_sms_brandname_client_rule.xml',
        'security/ir.model.access.csv',
        'views/send_sms_view.xml',
        'views/config_view.xml',
        'views/brand_name_view.xml',
        'views/send_sms_log_view.xml',
        'views/hr_employee_view.xml',
        'views/res_partner_view.xml',
        'views/menu_view.xml',
    ],
}
