# -*- coding: utf-8 -*-

from odoo import models, fields


class AutoSendSMSHPBD(models.Model):
    _name = 'at.brand_name'

    name = fields.Char(string='Tên thương hiệu', required=True)
    company_id = fields.Many2many('res.company', string='Công ty', required=True)
    default_brandname = fields.Boolean(string="Mặc định")
