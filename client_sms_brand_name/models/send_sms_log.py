# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class SendSMSLog(models.Model):
    _name = 'at.send_sms_log'

    name = fields.Char(string=_('Tiêu đề tin'), required=True)
    brand_name = fields.Many2one('at.brand_name', string=_('Thương hiệu'), required=True)
    sender = fields.Char(string=_('Người gửi'), default=lambda self: self.env.user.name, required=True)
    receiver = fields.Char(string=_('Người nhận'), required=True)
    send_time = fields.Datetime(string=_('Thời gian gửi'), required=True)
    content = fields.Text(string=_('Nội dung tin nhắn'), required=True)
    state = fields.Selection([('success', _('Thành công')), ('fail', _('Thất bại'))],
                             string=_('Trạng thái'))
    accented = fields.Boolean(string=_('Có dấu'))
    send_sms = fields.Many2one('at.send_sms')
